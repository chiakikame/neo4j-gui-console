= Neo4j GUI convenient interface (for embedded database)

This app is for opening a local neo4j database in embedded mode so that you can do simple inspection
without the need of setting up official neo4j distribution package.

== Running

To run this application, run the following command in the root of source tree

----
./gradlew run
----
