package org.thousandturtles

import org.neo4j.graphdb.GraphDatabaseService
import org.neo4j.graphdb.factory.GraphDatabaseFactory
import java.awt.Dimension
import java.awt.Font
import java.awt.TextField
import java.awt.event.ActionListener
import java.awt.event.KeyEvent
import java.awt.event.KeyListener
import java.io.File
import javax.swing.*

/*
 * Created by chiaki on 18/08/16.
 */
class MainWindow: JFrame() {
    val dbLocationTxt: JTextField
    val commandTxt: JTextArea
    val resultTxt: JTextArea

    val commandHistory = mutableListOf<String>()
    var currentCommandIdx = 0
    var db: GraphDatabaseService? = null
    val actionHandler: ActionListener

    init {
        defaultCloseOperation = JFrame.EXIT_ON_CLOSE

        actionHandler = ActionListener {actionEvent ->
            println(actionEvent.actionCommand)
            when (actionEvent.actionCommand) {
                "connect" -> connect()
                "disconnect" -> disconnect()
                "browse" -> browse()
                "run" -> run()
                "prev" -> prevCommand()
                "next" -> nextCommand()
                "clean" -> cleanHistory()
                else -> println("Command ${actionEvent.actionCommand} is not recognized")
            }
        }

        // Input part
        val pane = contentPane
        pane.layout = BoxLayout(pane, BoxLayout.Y_AXIS)

        pane.add(label("Databaase location"))
        dbLocationTxt = JTextField()
        dbLocationTxt.maximumSize = Dimension(dbLocationTxt.maximumSize.width, 64)
        dbLocationTxt.addKeyListener(MainWindow.DbPathKeyHandler(this))
        pane.add(dbLocationTxt)
        pane.add(dbCommandPane())
        pane.add(Box.createVerticalStrut(10))

        val monospaceFont = Font(Font.MONOSPACED, Font.PLAIN, 12)

        pane.add(label("Command"))
        commandTxt = JTextArea()
        commandTxt.addKeyListener(MainWindow.CommandKeyHandler(this))
        commandTxt.font = monospaceFont
        val cmdTxtScrollPane = JScrollPane(commandTxt)
        //cmdTxtScrollPane.maximumSize = commandTxt.maximumSize
        pane.add(cmdTxtScrollPane)
        pane.add(commandExecutionPane())
        pane.add(Box.createVerticalStrut(10))

        resultTxt = JTextArea()
        val txtScrollPane = JScrollPane(resultTxt)
        //txtScrollPane.maximumSize = txtScrollPane.maximumSize
        resultTxt.isEditable = false
        resultTxt.font = monospaceFont
        pane.add(txtScrollPane)
        //pane.add(Box.createVerticalGlue())

        title = "Simple Neo4j database interface"

        size = Dimension(480, 640)
        isVisible = true
        commandTxt.isEditable = false
        commandTxt.text = "Not yet connected"

        Runtime.getRuntime().addShutdownHook(Thread {
            this.db?.shutdown()
        })
    }

    private fun connect() {
        if (dbLocationTxt.text.isBlank()) {
            resultTxt.text = "Please fill or select a directory"
            return
        }

        try {
            db = GraphDatabaseFactory().newEmbeddedDatabase(File(dbLocationTxt.text))
            commandTxt.isEditable = true
            commandTxt.text = ""
            resultTxt.text = "Connection successful!"
        } catch (e: Exception) {
            resultTxt.text = e.stackTraceString()
        }
    }

    private fun disconnect() {
        this.db?.shutdown()
        this.db = null
        commandTxt.isEditable = false
        commandTxt.text = "Not yet connected"
        resultTxt.text = "Disconnection successful"
    }

    private fun browse() {
        val fc = JFileChooser()
        fc.fileSelectionMode = JFileChooser.DIRECTORIES_ONLY
        fc.isMultiSelectionEnabled = false
        if (fc.showDialog(this, "Select") == JFileChooser.APPROVE_OPTION) {
            dbLocationTxt.text = fc.selectedFile.absolutePath
        }
    }

    private fun run() {
        val db = this.db
        if (db == null) {
            resultTxt.text = "Not yet connected"
        } else {
            resultTxt.text = db.runQuery(commandTxt.text)
            commandHistory.add(commandTxt.text)
            currentCommandIdx = commandHistory.size
            commandTxt.text = ""
            println("idx: $currentCommandIdx, len: ${commandHistory.size}")
        }
    }

    private fun prevCommand() {
        println("idx: $currentCommandIdx, len: ${commandHistory.size}")
        if (currentCommandIdx - 1 < 0) {
            commandTxt.text = if (commandHistory.size == 0) "" else commandHistory.first()
        } else {
            currentCommandIdx -= 1
            commandTxt.text = commandHistory[currentCommandIdx]
        }
    }

    private fun nextCommand() {
        println("idx: $currentCommandIdx, len: ${commandHistory.size}")
        if (currentCommandIdx + 1 >= commandHistory.size) {
            commandTxt.text = ""
        } else {
            currentCommandIdx += 1
            commandTxt.text = commandHistory[currentCommandIdx]
        }
    }

    private fun cleanHistory() {
        commandHistory.clear()
        currentCommandIdx = 0
    }

    private fun commandExecutionPane(): JPanel {
        val pane = JPanel()
        val runBtn = JButton("Run")
        val prevBtn = JButton("Previous command")
        val nextBtn = JButton("Next command")
        val cleanBtn = JButton("Clean history")

        pane.layout = BoxLayout(pane, BoxLayout.X_AXIS)
        pane.add(runBtn)
        pane.add(Box.createHorizontalStrut(20))
        pane.add(prevBtn)
        pane.add(nextBtn)
        pane.add(cleanBtn)
        pane.add(Box.createHorizontalGlue())

        runBtn.actionCommand = "run"
        prevBtn.actionCommand = "prev"
        nextBtn.actionCommand = "next"
        cleanBtn.actionCommand = "clean"
        listOf(runBtn, prevBtn, nextBtn, cleanBtn).forEach { it.addActionListener(actionHandler) }
        return pane
    }

    private fun dbCommandPane(): JPanel {
        val pane = JPanel()
        val browseBtn = JButton("Browse")
        val connectBtn = JButton("Connect")
        val disconnectBtn = JButton("Disconnect")

        browseBtn.actionCommand = "browse"
        connectBtn.actionCommand = "connect"
        disconnectBtn.actionCommand = "disconnect"
        listOf(browseBtn, connectBtn, disconnectBtn).forEach {
            it.addActionListener(actionHandler)
        }

        pane.layout = BoxLayout(pane, BoxLayout.X_AXIS)
        pane.add(browseBtn)
        pane.add(connectBtn)
        pane.add(disconnectBtn)
        pane.add(Box.createHorizontalGlue())


        return pane
    }

    private fun label(label: String): JPanel {
        val lbl = JLabel(label)
        val pane = JPanel()
        pane.layout = BoxLayout(pane, BoxLayout.X_AXIS)
        pane.add(lbl)
        pane.add(Box.createHorizontalGlue())
        return pane
    }

    class DbPathKeyHandler(private val window: MainWindow): KeyListener {
        override fun keyTyped(p0: KeyEvent) {}

        override fun keyPressed(p0: KeyEvent) {
            if (p0.keyCode == KeyEvent.VK_ENTER) {
                window.connect()
            }
        }

        override fun keyReleased(p0: KeyEvent) {}
    }

    class CommandKeyHandler(private val window: MainWindow): KeyListener {
        override fun keyTyped(p0: KeyEvent) {}

        override fun keyPressed(p0: KeyEvent) {
            if (!p0.isControlDown) return

            if (p0.keyCode == KeyEvent.VK_ENTER) {
                window.run()
            } else if (p0.keyCode == KeyEvent.VK_UP || p0.keyCode == KeyEvent.VK_KP_UP) {
                window.prevCommand()
            } else if (p0.keyCode == KeyEvent.VK_DOWN || p0.keyCode == KeyEvent.VK_KP_DOWN) {
                window.nextCommand()
            }
        }

        override fun keyReleased(p0: KeyEvent) {}
    }
}