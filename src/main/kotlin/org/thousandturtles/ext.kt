package org.thousandturtles

import org.neo4j.graphdb.GraphDatabaseService
import java.io.PrintWriter
import java.io.StringWriter

/*
 * Created by chiaki on 18/08/16.
 */
fun GraphDatabaseService.runQuery(query: String): String {
    val tx = beginTx()
    val excp: Exception
    try {
        val r = execute(query)
        tx.success()
        return r.resultAsString()
    } catch(e: Exception) {
        tx.failure()
        excp = e
    } finally {
        tx.close()
    }
    return excp.toString()
}

fun Throwable.stackTraceString(): String{
    val writer = StringWriter()
    val pw = PrintWriter(writer)
    this.printStackTrace(pw)
    pw.flush()
    return writer.toString()
}